FROM openjdk:21-ea-14-jdk-slim
COPY target/artifactory-integration-0.0.1-SNAPSHOT.jar /artifactory.jar
EXPOSE 8080
CMD ["java", "-jar", "/artifactory.jar"]