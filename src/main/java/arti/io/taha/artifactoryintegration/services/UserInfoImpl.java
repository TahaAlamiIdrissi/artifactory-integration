package arti.io.taha.artifactoryintegration.services;

import arti.io.taha.artifactoryintegration.models.User;
import org.springframework.stereotype.Service;

@Service
public class UserInfoImpl implements UserInfo {
    private final static  double MIN_IMC = 18.5;
    private final static  double MAX_IMC = 24.9;
    @Override
    public boolean isUserHealthy(User user) {
        return user.calculateIMC() <= MAX_IMC && user.calculateIMC()>= MIN_IMC;
    }
}
