package arti.io.taha.artifactoryintegration.services;

import arti.io.taha.artifactoryintegration.models.User;

public interface UserInfo {
    boolean isUserHealthy(User user);
}
