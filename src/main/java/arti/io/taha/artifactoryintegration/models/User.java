package arti.io.taha.artifactoryintegration.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class User {
    private long id;
    private String name;
    private double weight;
    private double height;
    private double imc;
    private String phoneNumber;


    public double calculateIMC() {
        return this.weight/this.height*this.height;
    }

}
