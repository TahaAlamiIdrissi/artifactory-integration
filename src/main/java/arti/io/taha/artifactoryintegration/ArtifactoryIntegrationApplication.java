package arti.io.taha.artifactoryintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtifactoryIntegrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArtifactoryIntegrationApplication.class, args);
    }

}
