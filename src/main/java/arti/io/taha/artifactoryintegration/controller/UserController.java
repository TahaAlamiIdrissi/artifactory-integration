package arti.io.taha.artifactoryintegration.controller;

import arti.io.taha.artifactoryintegration.models.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/v1/")
@RestController
public class UserController {

    @GetMapping("index")
    public String index() {
        return "index";
    }

    @GetMapping("users")
    public ResponseEntity<User> getUser() {
        return ResponseEntity.ok(User.builder().weight(23.3).height(182.2).name("john").build());
    }
}
