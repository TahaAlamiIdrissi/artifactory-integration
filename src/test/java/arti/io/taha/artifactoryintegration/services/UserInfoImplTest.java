package arti.io.taha.artifactoryintegration.services;

import arti.io.taha.artifactoryintegration.models.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class UserInfoImplTest {

    @Test
    void testIsUserHealthy_whenUserIsHealthy() {
        // Arrange
        UserInfo userInfo = new UserInfoImpl();
        User mockUser = Mockito.mock(User.class);

        // Mock the behavior of calculateIMC
        when(mockUser.calculateIMC()).thenReturn(20.0);

        // Act
        boolean isHealthy = userInfo.isUserHealthy(mockUser);

        // Assert
        assertTrue(isHealthy, "User should be healthy");
    }

    @Test
    void testIsUserHealthy_whenUserIsNotHealthy() {
        // Arrange
        UserInfo userInfo = new UserInfoImpl();
        User mockUser = Mockito.mock(User.class);

        // Mock the behavior of calculateIMC
        when(mockUser.calculateIMC()).thenReturn(25.0);

        // Act
        boolean isHealthy = userInfo.isUserHealthy(mockUser);

        // Assert
        assertFalse(isHealthy, "User should not be healthy");
    }
}